# Code not highlighted

The code snippet is highlighted as expected when running `yarn dev`, however it is not after deploying to production.

```python
def greet(s):
    print(f"Hello, {s}")
```
