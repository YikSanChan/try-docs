const withNextra = require("nextra")("nextra-theme-docs", "./theme.config.js")
module.exports = withNextra({
  basePath: "/try-docs",
  webpack(config) {
    config.plugins.pop() // remove stork plugin which was added by nextra
    return config
  },
})
